//
//  ShortBuffer.hpp
//  OLA-C
//
//  Created by Shivansh on 7/4/17.
//  Copyright © 2017 TrillBit. All rights reserved.
//
#ifdef __cplusplus
#include <string>
#include <iostream>
using namespace std;
#endif

#ifndef ShortBuffer_hpp
#define ShortBuffer_hpp

#include <stdio.h>
#define TRIGGER_VALUE 75
//................Used for circular Buffer,i.e, Short Buffer
#define STORAGE_VECTOR 4
#define mBUFFERS_BEFORE_TRIGGER 2
#define CHUNK_AFTER_SAMPLE 28000 //@We divide this by 2 for use in the last function
#define TOTAL_CHUNKS_HAVING_DATA 23
#define DIVIDE_TOTAL_CHUNKS_INTO 3



class ShortBuffer {
    std::string storage[TOTAL_CHUNKS_HAVING_DATA];            //@storage is the final array of chunks of our whole data
    int flag;                                                 //@flag , 0:continue recording, 1: Trigger detect, 2:Correlation calculation
    int storagect;
    string c_buffer[STORAGE_VECTOR];
    
    string allBufferString ;                                 //@allBufferString is used here to save storage array in one vector
    string allpairDecoded ;
public:
    ShortBuffer()  //Constructor
    {flag=0; storagect=0;}
    
    int putshortsinarray(string const& shorts, int &buffer_count, double result);
    int WritetoStorage(int buffer_count);
    int divideDataby();
    //@ putshortsinarray() - Puts data into temporary array buffer,c_buffer
    //@ WritetoStorage() - After trigger is detected, the subsequent TOTAL_CHUNKS_HAVING_DATA are recorded into storage array
    //@ divideDataby() - Divides total data to be taken in a few parts, and retirieves data
};

#endif /* ShortBuffer_hpp */
