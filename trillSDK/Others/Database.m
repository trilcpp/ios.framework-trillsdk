    
//
//  Database.m
//  trillSDK
//
//  Created by Mac Mini on 11/30/17.
//  Copyright © 2017 TrillBit. All rights reserved.
//

#import "Database.h"
#import <FMDB.h>

#define DATABASE_VERSION 1
#define URLhit @"https://api.trillbit.com/v1"

@implementation Database

//Symbols for Database
@synthesize databaseName = _databaseName;
@synthesize databasePath = _databasePath;
@synthesize sdk_token = _sdk_token;

//----Loads and Checks if database is created, and downloads data
-(void)loadDatabase: (NSString *) API_token {
        //...SQLite
        Database *globals = [Database sharedInstance];
        globals.sdk_token = API_token;
        globals.databaseName = @"coupons.db";
        NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
        NSString *documentsDir = [docPaths objectAtIndex:0];
        globals.databasePath = [documentsDir stringByAppendingPathComponent:globals.databaseName];
        [self createAndCheckDatabase];
    //---Set Database version
    NSFileManager *file = [NSFileManager defaultManager];
    FMDatabase *database = [FMDatabase databaseWithPath:globals.databasePath];
    [database open];
    uint32_t new_version = DATABASE_VERSION;
    [database setUserVersion:new_version];  //--Set new_user_version
    [database close];
        //---Fetch Information
        [self jsonFetch:[ NSString stringWithFormat: @"%@\/get_all_info?all=true",URLhit] withCompletionHandler:^{
            [self jsonFetch:[ NSString stringWithFormat: @"%@\/stores?all=true",URLhit] withCompletionHandler:^{}];
    }];
}

-(void)loadDatabase {
    //--SQLite
    Database *globals = [Database sharedInstance];
    globals.databaseName = @"coupons.db";
    NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [docPaths objectAtIndex:0];
    globals.databasePath = [documentsDir stringByAppendingPathComponent:globals.databaseName];
    
    //----Check Database version number
    NSFileManager *file = [NSFileManager defaultManager];
    FMDatabase *database = [FMDatabase databaseWithPath:globals.databasePath];
    [database open];
    uint32_t current_version = [database userVersion];
    uint32_t new_version = DATABASE_VERSION;
    [database setUserVersion:new_version];  //--Set new_user_version
    if(new_version>current_version){
        //Delete
        NSFileManager *file = [NSFileManager defaultManager];
        [file removeItemAtPath:globals.databasePath error:nil];
        //Redirect to [self load_database: sdk_token]
        NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
        NSString *sdk_token = [preferences objectForKey:@"sdk_token"] ;
        [self loadDatabase: sdk_token];
        return;
    }

    [self createAndCheckDatabase];
    //---Enable to Fetch Information everytime App gets on
    [self jsonFetch:[ NSString stringWithFormat: @"%@\/get_all_info",URLhit] withCompletionHandler:^{ //----> the URL being hit is get_all_info ONLY, and not get_all_info?all=true
        [self jsonFetch:[ NSString stringWithFormat: @"%@\/stores",URLhit] withCompletionHandler:^{}];
    }];
}

+ (Database *) sharedInstance{
    static dispatch_once_t onceToken;
    static Database *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[Database alloc] init];
    });
    return instance;
}

//------
#pragma mark - Database
//------
-(void) createAndCheckDatabase{
    BOOL success=NO;
    NSFileManager *file = [NSFileManager defaultManager];
    Database *globals = [Database sharedInstance];
    success = [file fileExistsAtPath:globals.databasePath];
    
    //----initiate the tables
    [Database get_couponinfo];
    [Database get_zoneinfo];
    [Database get_notificationinfo];
    [Database get_trilytics];
    [Database get_categoryinfo];
    [Database get_syncdata];
    [Database get_storedata];
    [Database get_zonemap];
    
    if(success) {
        NSLog(@"<Database exists>");
        return;
    }
    //If database doesn't exist, copy it from mainBundle and put it in the Directory of Device
    NSString *databasePathFromApp = [ [[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:globals.databaseName];
    [file copyItemAtPath:databasePathFromApp toPath:globals.databasePath error:nil];
    
    //Creating Tables in the new database if it doesn't already exist
    FMDatabase *database = [FMDatabase databaseWithPath:globals.databasePath];
    [database open];
    
    NSString *sql_query;
    //----TABLE for coupon_info details1
    sql_query=[NSString stringWithFormat:@"CREATE TABLE %@ (%@ TEXT PRIMARY KEY DEFAULT NULL, %@ TEXT DEFAULT NULL,%@ INTEGER default NULL, %@ TEXT , %@ TEXT DEFAULT NULL, %@ TEXT, %@ TEXT DEFAULT NULL, %@ TEXT, %@ TEXT, %@ TEXT, %@ INTEGER DEFAULT NULL, %@ INTEGER DEFAULT NULL, %@ INTEGER DEFAULT NULL , %@ INTEGER DEFAULT NULL , %@ INTEGER DEFAULT NULL, %@ TEXT , %@ TEXT )",[_coupon_info objectAtIndex:0], [_coupon_info objectAtIndex:1], [_coupon_info objectAtIndex:2], [_coupon_info objectAtIndex:3], [_coupon_info objectAtIndex:4], [_coupon_info objectAtIndex:5], [_coupon_info objectAtIndex:6], [_coupon_info objectAtIndex:7], [_coupon_info objectAtIndex:8], [_coupon_info objectAtIndex:9], [_coupon_info objectAtIndex:10], [_coupon_info objectAtIndex:11], [_coupon_info objectAtIndex:12], [_coupon_info objectAtIndex:13], [_coupon_info objectAtIndex:14], [_coupon_info objectAtIndex:15], [_coupon_info objectAtIndex:16], [_coupon_info objectAtIndex:17] ];
    [database executeUpdate:sql_query];

    //----TABLE for ZoneInfo2
    sql_query=[NSString stringWithFormat:@"CREATE TABLE %@ (%@ TEXT PRIMARY KEY DEFAULT NULL, %@ TEXT DEFAULT NULL,%@ TEXT default NULL, %@ TEXT , %@ INTEGER, %@ INTEGER , %@ TEXT, %@ TEXT, %@ INTEGER DEFAULT NULL)",[_zone_info objectAtIndex:0], [_zone_info objectAtIndex:1], [_zone_info objectAtIndex:2], [_zone_info objectAtIndex:3], [_zone_info objectAtIndex:4], [_zone_info objectAtIndex:5], [_zone_info objectAtIndex:6], [_zone_info objectAtIndex:7], [_zone_info objectAtIndex:8], [_zone_info objectAtIndex:9] ];
    NSLog(@"\n--ZoneInfo %@",sql_query);
    [database executeUpdate:sql_query];
    
    //----TABLE for NotificationInfo3
    sql_query=[NSString stringWithFormat:@"CREATE TABLE %@ (%@ TEXT PRIMARY KEY DEFAULT NULL, %@ TEXT DEFAULT NULL,%@ TEXT default NULL, %@ TEXT , %@ TEXT DEFAULT NULL, %@ TEXT DEFAULT NULL , %@ TEXT, %@ TEXT)",[_notification_info objectAtIndex:0], [_notification_info objectAtIndex:1], [_notification_info objectAtIndex:2], [_notification_info objectAtIndex:3], [_notification_info objectAtIndex:4], [_notification_info objectAtIndex:5], [_notification_info objectAtIndex:6], [_notification_info objectAtIndex:7], [_notification_info objectAtIndex:8] ];
    NSLog(@"\n--NotificationInfo %@",sql_query);
    [database executeUpdate:sql_query];
    
    //----TABLE for trilytics4
    sql_query=[NSString stringWithFormat:@"CREATE TABLE %@ (%@ TEXT DEFAULT NULL)",[_trilytics objectAtIndex:0], [_trilytics objectAtIndex:1] ];
    NSLog(@"\n--trilytics %@",sql_query);
    [database executeUpdate:sql_query];
    
    //----TABLE for category5
    sql_query=[NSString stringWithFormat:@"CREATE TABLE %@ (%@ TEXT PRIMARY KEY DEFAULT NULL, %@ TEXT DEFAULT NULL, %@ TEXT DEFAULT NULL)",[_category objectAtIndex:0], [_category objectAtIndex:1], [_category objectAtIndex:2], [_category objectAtIndex:3] ];
    NSLog(@"\n--category%@",sql_query);
    [database executeUpdate:sql_query];
    
    //----TABLE for coupon_category6
    [Database get_couponcategory];
    sql_query=[NSString stringWithFormat:@"CREATE TABLE %@ (%@ TEXT PRIMARY KEY DEFAULT NULL, %@ TEXT DEFAULT NULL, %@ TEXT DEFAULT NULL)",[_coupon_category objectAtIndex:0], [_coupon_category objectAtIndex:1], [_coupon_category objectAtIndex:2], [_coupon_category objectAtIndex:3] ];
    NSLog(@"\n--COUPON_category%@",sql_query);
    [database executeUpdate:sql_query];
    
    //----TABLE for sync_data7
    sql_query=[NSString stringWithFormat:@"CREATE TABLE %@ (%@ TEXT PRIMARY KEY DEFAULT NULL, %@ TEXT DEFAULT NULL, %@ TEXT DEFAULT NULL, %@ TEXT DEFAULT NULL)",[_sync_data objectAtIndex:0], [_sync_data objectAtIndex:1], [_sync_data objectAtIndex:2], [_sync_data objectAtIndex:3], [_sync_data objectAtIndex:4]  ];
    NSLog(@"\n--sync_data%@",sql_query);
    [database executeUpdate:sql_query];
    
    //----TABLE for store_data8
    sql_query=[NSString stringWithFormat:@"CREATE TABLE %@ (%@ TEXT DEFAULT NULL, %@ TEXT PRIMARY KEY DEFAULT NULL, %@ TEXT DEFAULT NULL, %@ TEXT DEFAULT NULL, %@ TEXT DEFAULT NULL, %@ TEXT default NULL)",[_store_data objectAtIndex:0], [_store_data objectAtIndex:1], [_store_data objectAtIndex:2], [_store_data objectAtIndex:3], [_store_data objectAtIndex:4], [_store_data objectAtIndex:5],[_store_data objectAtIndex:6] ];
    NSLog(@"\n--store_data%@",sql_query);
    [database executeUpdate:sql_query];
    
    //----TABLE for zone_map9
    sql_query=[NSString stringWithFormat:@"CREATE TABLE %@ (%@ TEXT DEFAULT NULL, %@ TEXT DEFAULT NULL, %@ TEXT DEFAULT NULL, %@ TEXT DEFAULT NULL)",[_zone_map objectAtIndex:0], [_zone_map objectAtIndex:1], [_zone_map objectAtIndex:2], [_zone_map objectAtIndex:3], [_zone_map objectAtIndex:4] ];
    NSLog(@"\n--zone_map%@",sql_query);
    [database executeUpdate:sql_query];

    [database close];
    //--If database didn't exist and was just created as above, then data is also downloaded
    NSLog(@"\n\n\n\n--database/createAndCheckDatabase -> Fetching data");
    
}

//------------------------------------------------------------------------------
#pragma mark - Inserting JSON
//------------------------------------------------------------------------------

-(BOOL) saveApiResults: (NSDictionary *)payload{
    //assign false value to saveSuccess
    BOOL saveSuccess = NO;
    
    //----Check wheter payload is empty or not
    if( [payload count]==0 ) return YES;
    
    //----Check wheter storeData or not
    NSDictionary *store_new = [payload objectForKey:@"store_new"];
    NSDictionary *store_deleted = [payload objectForKey:@"store_deleted"];
    NSDictionary *store_updated = [payload objectForKey:@"store_updated"];
    //--Parsing STORE details
    if( [store_new count]!=0 ) saveSuccess = [self insertInTable:[_store_data objectAtIndex:0]:store_new];
    if( [store_updated count]!=0 ) [self updateTable:[_store_data objectAtIndex:0] :store_updated];
    if( [store_deleted count]!=0 ) [self deleteTableRows:[_store_data objectAtIndex:0] :store_deleted];
    
    //-------Fetch coupon_info1
    NSDictionary *coupon_info = [payload objectForKey:[_coupon_info objectAtIndex:0]];
    NSDictionary *coupon_new = [coupon_info objectForKey:@"coupon_new"];
    NSDictionary *coupon_updated = [coupon_info objectForKey:@"coupon_updated"];
    NSDictionary *coupon_deleted = [coupon_info objectForKey:@"coupon_deleted"];
    //--Parsing coupon details
    if( [coupon_new count]!=0 ) saveSuccess = [self insertInTable:[_coupon_info objectAtIndex:0] :coupon_new];
    if( [coupon_updated count]!=0 ) [self updateTable:[_coupon_info objectAtIndex:0] :coupon_updated];
    if( [coupon_deleted count]!=0 ) [self deleteTableRows:[_coupon_info objectAtIndex:0] :coupon_deleted];
    
    //-------Fetch zone_info2
    NSDictionary *zone_info = [payload objectForKey:[_zone_info objectAtIndex:0]];
    NSDictionary *zone_new = [zone_info objectForKey:@"zone_new"];
    NSDictionary *zone_updated = [zone_info objectForKey:@"zone_updated"];
    NSDictionary *zone_deleted = [zone_info objectForKey:@"zone_deleted"];
    //--Parsing zone details
    if( [zone_new count]!=0 ) saveSuccess =  [self insertInTable:[_zone_info objectAtIndex:0] :zone_new];
    if( [zone_updated count]!=0 ) [self updateTable:[_zone_info objectAtIndex:0] :zone_updated];
    if( [zone_deleted count]!=0 ) [self deleteTableRows:[_zone_info objectAtIndex:0] :zone_deleted];
    
    //-------Fetch category3
    NSDictionary *category_info = [payload objectForKey:[_category objectAtIndex:0]];
    //--Parsing zone details
    if( [category_info count]!=0 ) saveSuccess =  [self insertInTable:[_category objectAtIndex:0] :category_info];
    
    //-------Fetch coupon_category4
    NSDictionary *coupon_category_info = [payload objectForKey:[_coupon_category objectAtIndex:0]];
    //--Parsing zone details
    if( [coupon_category_info count]!=0 ) saveSuccess =  [self insertInTable:[_coupon_category objectAtIndex:0] :coupon_category_info];

    //-------Fetch zone_map5
    NSDictionary *zone_map = [payload objectForKey:[_zone_map objectAtIndex:0]];
    NSDictionary *map_new = [zone_map objectForKey:@"map_new"];
    NSDictionary *map_updated = [zone_map objectForKey:@"map_updated"];
    NSDictionary *map_deleted = [zone_map objectForKey:@"map_deleted"];
    //--Parsing coupon details
    if( [map_new count]!=0 ) saveSuccess = [self insertInTable:[_zone_map objectAtIndex:0] :map_new];
    if( [map_updated count]!=0 ) [self updateTable:[_zone_map objectAtIndex:0] :map_updated];
    if( [map_deleted count]!=0 ) [self deleteTableRows:[_zone_map objectAtIndex:0] :map_deleted];
    
    return saveSuccess;
}

-(BOOL) insertInTable: (NSString *)tableName : (NSDictionary *)jsondata{
    NSLog(@"\n\n-----------------INSERT IN TABLE %@",tableName);
    BOOL saveSuccess = NO;
    Database *globals = [Database sharedInstance];
    FMDatabase *database = [FMDatabase databaseWithPath:globals.databasePath];
    [database open];
    NSString *sql_query;
    if( [tableName isEqual: ([_coupon_info objectAtIndex:0])] ){//1
        NSLog(@"\nEntered coupon_info");
        for(NSDictionary *new_array in jsondata) {
            //--Adding key values from new_array where keys are as in _coupon_info table, starting from 1
            //2 changes, i.e, _coupon_info used below 3 times
            NSMutableArray *values = [[NSMutableArray alloc]init];
            NSString *question_marks = [[NSString alloc] init];
            int size = [_coupon_info count];//
            for (int i=1; i<size; i++){
                [values addObject:[new_array objectForKey:[_coupon_info objectAtIndex:i]]];//
                question_marks = [question_marks stringByAppendingString:@"?"];
                if(i<size-1) question_marks = [question_marks stringByAppendingString:@","];
            }
            sql_query = [NSString stringWithFormat:@"INSERT INTO %@ VALUES (%@)", tableName, question_marks];
            saveSuccess = [database executeUpdate:sql_query withArgumentsInArray:values];
        }
    } else if( [tableName isEqual: ([_zone_info objectAtIndex:0])] ){//2
        NSLog(@"\nEntered zone_info");
        for(NSDictionary *new_array in jsondata) {
            //--Adding key values from new_array where keys are as in _coupon_info table, starting from 1
            //2 changes, i.e, "_zone_info" used below 3 times
            NSMutableArray *values = [[NSMutableArray alloc]init];
            NSString *question_marks = [[NSString alloc] init];
            int size = [_zone_info count];//
            for (int i=1; i<size; i++){
                [values addObject:[new_array objectForKey:[_zone_info objectAtIndex:i]]];//
                question_marks = [question_marks stringByAppendingString:@"?"];
                if(i<size-1) question_marks = [question_marks stringByAppendingString:@","];
            }
            sql_query = [NSString stringWithFormat:@"INSERT INTO %@ VALUES (%@)", tableName, question_marks];
            saveSuccess = [database executeUpdate:sql_query withArgumentsInArray:values];
        }
    } else if( [tableName isEqual: ([_category objectAtIndex:0])] ){//3
        NSLog(@"\nEntered category");
        for(NSDictionary *new_array in jsondata) {
            //--Adding key values from new_array where keys are as in _coupon_info table, starting from 1
            //2 changes
            NSMutableArray *values = [[NSMutableArray alloc]init];
            NSString *question_marks = [[NSString alloc] init];
            int size = [_category count];//
            for (int i=1; i<size; i++){
                [values addObject:[new_array objectForKey:[_category objectAtIndex:i]]];//
                question_marks = [question_marks stringByAppendingString:@"?"];
                if(i<size-1) question_marks = [question_marks stringByAppendingString:@","];
            }
            sql_query = [NSString stringWithFormat:@"INSERT INTO %@ VALUES (%@)",tableName,question_marks];
            saveSuccess = [database executeUpdate:sql_query withArgumentsInArray:values];
        }
    } else if( [tableName isEqual: ([_coupon_category objectAtIndex:0])] ){//4
        NSLog(@"\nEntered coupon_category");
        for(NSDictionary *new_array in jsondata) {
            //2 changes, i.e, "_zone_info" used below 3 times
            NSMutableArray *values = [[NSMutableArray alloc]init];
            NSString *question_marks = [[NSString alloc] init];
            int size = [_coupon_category count];//
            for (int i=1; i<size; i++){
                [values addObject:[new_array objectForKey:[_coupon_category objectAtIndex:i]]];//
                question_marks = [question_marks stringByAppendingString:@"?"];
                if(i<size-1) question_marks = [question_marks stringByAppendingString:@","];
            }
            sql_query = [NSString stringWithFormat:@"INSERT INTO %@ VALUES (%@)", tableName,question_marks];
            saveSuccess = [database executeUpdate:sql_query withArgumentsInArray:values];
        }
    } else if( [tableName isEqual: ([_store_data objectAtIndex:0])] ){//5
        NSLog(@"\nEntered store_data");
        for(NSDictionary *new_array in jsondata) {
            //2 changes
            NSMutableArray *values = [[NSMutableArray alloc]init];
            NSString *question_marks = [[NSString alloc] init];
            int size = [_store_data count];//
            for (int i=1; i<size; i++){
                [values addObject:[new_array objectForKey:[_store_data objectAtIndex:i]]];//
                question_marks = [question_marks stringByAppendingString:@"?"];
                if(i<size-1) question_marks = [question_marks stringByAppendingString:@","];
            }
            sql_query = [NSString stringWithFormat:@"INSERT INTO %@ VALUES (%@)", tableName, question_marks];
            saveSuccess = [database executeUpdate:sql_query withArgumentsInArray:values];
        }
    } else if( [tableName isEqual: ([_notification_info objectAtIndex:0])] ){//6
        NSLog(@"\nEntered notification");
        for(NSDictionary *new_array in jsondata) {
            //2 changes
            NSMutableArray *values = [[NSMutableArray alloc]init];
            NSString *question_marks = [[NSString alloc] init];
            int size = [_notification_info count];//
            for (int i=1; i<size; i++){
                [values addObject:[new_array objectForKey:[_notification_info objectAtIndex:i]]];//
                question_marks = [question_marks stringByAppendingString:@"?"];
                if(i<size-1) question_marks = [question_marks stringByAppendingString:@","];
            }
            sql_query = [NSString stringWithFormat:@"INSERT INTO %@ VALUES (%@)", tableName, question_marks];
            saveSuccess = [database executeUpdate:sql_query withArgumentsInArray:values];
        }
    } else if( [tableName isEqual: ([_trilytics objectAtIndex:0])] ){//7
        NSLog(@"\nEntered trilytics");
        for(NSDictionary *new_array in jsondata) {
            //2 changes
            NSMutableArray *values = [[NSMutableArray alloc]init];
            NSString *question_marks = [[NSString alloc] init];
            int size = [_trilytics count];//
            for (int i=1; i<size; i++){
                [values addObject:[new_array objectForKey:[_trilytics objectAtIndex:i]]];//
                question_marks = [question_marks stringByAppendingString:@"?"];
                if(i<size-1) question_marks = [question_marks stringByAppendingString:@","];
            }
            sql_query = [NSString stringWithFormat:@"INSERT INTO %@ VALUES (%@)", tableName, question_marks];
            saveSuccess = [database executeUpdate:sql_query withArgumentsInArray:values];
        }
    } else if( [tableName isEqual: ([_sync_data objectAtIndex:0])] ){//8
        NSLog(@"\nEntered trilytics");
        for(NSDictionary *new_array in jsondata) {
            //2 changes
            NSMutableArray *values = [[NSMutableArray alloc]init];
            NSString *question_marks = [[NSString alloc] init];
            int size = [_sync_data count];//
            for (int i=1; i<size; i++){
                [values addObject:[new_array objectForKey:[_sync_data objectAtIndex:i]]];//
                question_marks = [question_marks stringByAppendingString:@"?"];
                if(i<size-1) question_marks = [question_marks stringByAppendingString:@","];
            }
            sql_query = [NSString stringWithFormat:@"INSERT INTO %@ VALUES (%@)", tableName, question_marks];
            saveSuccess = [database executeUpdate:sql_query withArgumentsInArray:values];
        }
    }
    else if( [tableName isEqual: ([_zone_map objectAtIndex:0])] ){//9
        NSLog(@"\nEntered zone_map");
        for(NSDictionary *new_array in jsondata) {
            //2 changes
            NSMutableArray *values = [[NSMutableArray alloc]init];
            NSString *question_marks = [[NSString alloc] init];
            int size = [_zone_map count];//
            for (int i=1; i<size; i++){
                [values addObject:[new_array objectForKey:[_zone_map objectAtIndex:i]]];//
                question_marks = [question_marks stringByAppendingString:@"?"];
                if(i<size-1) question_marks = [question_marks stringByAppendingString:@","];
            }
            sql_query = [NSString stringWithFormat:@"INSERT INTO %@ VALUES (%@)", tableName, question_marks];
            saveSuccess = [database executeUpdate:sql_query withArgumentsInArray:values];
        }
    }
    
    [database close];
    return saveSuccess;
}


-(void) insertInSyncTable:(NSDictionary *)dictionary {

    NSLog(@"\n\n-----------------INSERT VALUES IN SYNC TABLE");
    Database *globals = [Database sharedInstance];
    FMDatabase *database = [FMDatabase databaseWithPath:globals.databasePath];
    [database open];
    BOOL saveSuccess = NO;
    
    NSString *colName = [[NSString alloc]init];
    NSMutableArray *values = [[NSMutableArray alloc]init];
    NSString *question_marks = [[NSString alloc] init];
    NSMutableArray *dict_keys = [NSMutableArray arrayWithArray:[dictionary allKeys]];
    int size = [dictionary count];
    for(int i=0; i<size; i++){
        if([_sync_data containsObject:[dict_keys objectAtIndex:i]]){
            colName = [colName stringByAppendingString:[dict_keys objectAtIndex:i]];
            [values addObject:[dictionary valueForKey:[dict_keys objectAtIndex:i]]];
            question_marks = [question_marks stringByAppendingString:@"?"];
            if(i<size-1) {
                question_marks = [question_marks stringByAppendingString:@","];
                colName = [colName stringByAppendingString:@","];
            }
        }//@end if sync_data
    }//@end for
    
    NSString *sql_query = [NSString stringWithFormat:@"INSERT OR REPLACE INTO sync_data (%@) VALUES(%@)", colName, question_marks];
    saveSuccess = [database executeUpdate:sql_query withArgumentsInArray:values];
    
    [database close];
}
//------------------------
#pragma mark - Delete
//------------------

-(BOOL) deleteTableRows : (NSString *)tableName : (NSDictionary *)jsondata{
    NSLog(@"\n\n-----------------DELETE VALUES IN TABLE");
    BOOL success = NO;
    Database *globals = [Database sharedInstance];
    FMDatabase *database = [FMDatabase databaseWithPath:globals.databasePath];
    [database open];
    NSString *sql_query;
    
    if( [tableName isEqual: [_coupon_info objectAtIndex:0]] ){
        for(NSDictionary *new_array in jsondata) {
            sql_query = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@= ?",tableName,[_coupon_info objectAtIndex:1]];
            success = [database executeUpdate:sql_query, [new_array objectForKey:[_coupon_info objectAtIndex:1]], nil];
        }
    }
    else if( [tableName isEqual: [_zone_info objectAtIndex:0]] ){
        for(NSDictionary *new_array in jsondata) {
            sql_query = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@= ?",tableName,[_zone_info objectAtIndex:1]];
            success = [database executeUpdate:sql_query, [new_array objectForKey:[_zone_info objectAtIndex:1]], nil];
        }
    }
    else if( [tableName isEqual: [_store_data objectAtIndex:0]] ){
        for(NSDictionary *new_array in jsondata) {
            sql_query = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@= ?",tableName,[_store_data objectAtIndex:2]];
            success = [database executeUpdate:sql_query, [new_array objectForKey:[_store_data objectAtIndex:2]], nil];
        }
    }
    else if( [tableName isEqual: [_zone_map objectAtIndex:0]] ){
        for(NSDictionary *new_array in jsondata) {
            sql_query = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@= ?",tableName,[_zone_map objectAtIndex:1]];
            success = [database executeUpdate:sql_query, [new_array objectForKey:[_zone_map objectAtIndex:1]], nil];
        }
    }
    [database close];
    return success;
}


-(BOOL) deleteSyncTable {
    NSLog(@"\n\n-----------------DELETE SYNC IN TABLE");
    BOOL success = NO;
    Database *globals = [Database sharedInstance];
    FMDatabase *database = [FMDatabase databaseWithPath:globals.databasePath];
    [database open];
    NSString *sql_query;
    
    sql_query = @"DELETE FROM sync_data";
    success = [database executeUpdate:sql_query];

    [database close];
    return success;
}

//------------------------
#pragma mark - display Results
//------------------
-(NSArray*)displayTable:(NSString*)tableName columnName:(NSString *)columnName{
    Database *globals = [Database sharedInstance];
  
      FMDatabase *database = [FMDatabase databaseWithPath:globals.databasePath];
      if(![database open])
          return nil;
    
      [database open];
    
      NSString *query=[NSString stringWithFormat:@"SELECT * FROM %@",tableName];
    
      FMResultSet *results = [database executeQuery: query];
      // Or with variables FMResultSet *results = [database executeQuery:@"SELECT * from tableName where fieldName= ?",[NSString stringWithFormat:@"%@", variableName]];
      NSArray *resultDataSet=[NSArray array];
      while([results next]) {
          NSString *name = [results stringForColumn:columnName];
          resultDataSet = [resultDataSet arrayByAddingObject:name ];
      }
  
    [database close];
    return resultDataSet;
}




-(NSDictionary*)displayTable: (NSString*)tableName preCondition:(NSString *)precondition selectfromTableNamePLUSCondition:(NSString *)condition
{   Database *globals = [Database sharedInstance];
    FMDatabase *database = [FMDatabase databaseWithPath:globals.databasePath];
    if(![database open])
        return nil;
    [database open];
    
    NSString *query;
    //--PreCondition
    if(precondition==nil)
        query = [NSString stringWithFormat:@"SELECT *"];
    else
        query = [NSString stringWithFormat:@"%@",precondition];
    //--Condition
    if (condition == nil)
        query=[NSString stringWithFormat:@"%@ FROM %@",query,tableName];
    else
        query=[NSString stringWithFormat:@"%@ FROM %@ %@",query,tableName,condition];
    
    FMResultSet *results = [database executeQuery: query];
    // Or with variables FMResultSet *results = [database executeQuery:@"SELECT * from tableName where fieldName= ?",[NSString stringWithFormat:@"%@", variableName]];
    NSMutableDictionary *resultDataSet=[[NSMutableDictionary alloc]init];
    int key_count=0;
    while([results next]) {
        NSMutableDictionary *row=[[NSMutableDictionary alloc]init];
        for(int i=0; i<[results columnCount]; i++){
//            NSLog(@"\n--%@",[results columnNameForIndex:i]);
            NSString *columnKey = [NSString stringWithFormat:@"%@",[results columnNameForIndex:i] ];
            NSString *value = [results stringForColumn:columnKey];
            [row setValue:value forKey:columnKey];
        }
        //--Check EXPIRY if table=coupon_info
        if([tableName isEqualToString: [_coupon_info objectAtIndex:0] ]){
            NSString *EXPIRY = [results stringForColumn:[NSString stringWithFormat:@"%@", [_coupon_info objectAtIndex:10]]];
            if([self couponExpiry:EXPIRY]==NO){
                [resultDataSet setObject:row forKey:[NSString stringWithFormat:@"%d", key_count] ];
                key_count+=1;
            }
        } else{
            [resultDataSet setObject:row forKey:[NSString stringWithFormat:@"%d", key_count] ];
            key_count+=1;
        }
    }
    NSLog(@"\n\n ResultDataSet : %@",resultDataSet);
    [database close];
    return resultDataSet;
}

-(NSDictionary*)fetchCodeDictionary:(int)code islocation:(BOOL)isLocation
{   Database *globals = [Database sharedInstance];
    //Query
    NSString *sql;
    if(isLocation == NO)
        sql=[NSString stringWithFormat:@"SELECT * FROM %@ WHERE tone = %d ", [_coupon_info objectAtIndex:0], code];
    else
        sql=[NSString stringWithFormat:@"SELECT * FROM %@ WHERE zone_code = %d ", [_zone_info objectAtIndex:0], code];
    
    FMDatabase *database = [FMDatabase databaseWithPath:globals.databasePath];
    if(![database open]) {
        return nil;
    }
    [database open];
    FMResultSet *results = [database executeQuery:sql];
    NSLog(@"\n.........fetchCodeDictionary, %@, result.......\n.",globals.databasePath);
    
    NSMutableDictionary *resultDataSet=[[NSMutableDictionary alloc]init];
    int key_count=0;
    while([results next]) {
        NSMutableDictionary *row=[[NSMutableDictionary alloc]init];
        NSString *name = [results stringForColumn:@"brand_name"];
        NSLog(@"%@ columnCount:%d",name,[results columnCount]);
        for(int i=0; i<[results columnCount]; i++){
            //NSLog(@"\n--%@",[results columnNameForIndex:i]);
            NSString *columnKey = [NSString stringWithFormat:@"%@",[results columnNameForIndex:i] ];
            NSString *value = [results stringForColumn:columnKey];
            [row setValue:value forKey:columnKey];
        }
        //--Check EXPIRY iF Table is coupon_info
        if(isLocation ==NO){
            NSString *EXPIRY = [results stringForColumn:[NSString stringWithFormat:@"%@", [_coupon_info objectAtIndex:10]]];
            if([self couponExpiry:EXPIRY]==NO){
                [resultDataSet setObject:row forKey:[NSString stringWithFormat:@"%d", key_count] ];
                key_count+=1;
            }
        } else{
            [resultDataSet setObject:row forKey:[NSString stringWithFormat:@"%d", key_count] ];
            key_count+=1;
        }
    }
    [database close];
    //---If number doesn't exist, check Server
    if([resultDataSet count]==0)
        [self jsonFetch: [ NSString stringWithFormat: @"%@\/get_all_info",URLhit] withCompletionHandler:^{}];
    
    return resultDataSet;
}

//------------------------
#pragma mark - Updated
//------------------
-(BOOL) updateTable : (NSString *)tableName : (NSDictionary *)jsondata{
    NSLog(@"\n\n-----------------UPDATE TABLE");
    BOOL success = NO;
    Database *globals = [Database sharedInstance];
    FMDatabase *database = [FMDatabase databaseWithPath:globals.databasePath];
    [database open];
    NSString *sql_query;
    
    if( [tableName isEqual: [_coupon_info objectAtIndex:0]] ){//1
        for(NSDictionary *new_array in jsondata) {
            sql_query =[NSString stringWithFormat:@"UPDATE %@ SET ",tableName];
            NSMutableArray *values = [[NSMutableArray alloc]init];
            NSMutableArray *keys_of_dict = [NSMutableArray arrayWithArray:[new_array allKeys]];
            if([keys_of_dict containsObject:[_coupon_info objectAtIndex:1]])
                [keys_of_dict removeObject:[_coupon_info objectAtIndex:1]]; //Removing @"id" since it's unique
            int size = [keys_of_dict count];//Incase all values we don't recieve for updation, we update what we get
            for (int i=0; i<size; i++){
                if([_coupon_info containsObject:[keys_of_dict objectAtIndex:i]]) {//If matching Values are there
                    sql_query = [sql_query stringByAppendingString:[NSString stringWithFormat:@"%@=?",[keys_of_dict objectAtIndex:i]]];//
                    [values addObject:[new_array objectForKey:[keys_of_dict objectAtIndex:i]]];// ? mark values go here
                    if(i<size-1) sql_query = [sql_query stringByAppendingString:@","];
                }//@end if
            }//@end for values insertion
            
            sql_query = [sql_query stringByAppendingString:[NSString stringWithFormat:@" WHERE %@=?",[_coupon_info objectAtIndex:1]]];//
            [values addObject:[new_array objectForKey:[_coupon_info objectAtIndex:1]]];
            success = [database executeUpdate:sql_query withArgumentsInArray:values];
        }
    }
    else if( [tableName isEqual: [_zone_info objectAtIndex:0]] ){
        for(NSDictionary *new_array in jsondata) {
            sql_query =[NSString stringWithFormat:@"UPDATE %@ SET ",tableName];
            NSMutableArray *values = [[NSMutableArray alloc]init];
            NSMutableArray *keys_of_dict = [NSMutableArray arrayWithArray:[new_array allKeys]];
            if([keys_of_dict containsObject:[_zone_info objectAtIndex:1]])
                [keys_of_dict removeObject:[_zone_info objectAtIndex:1]]; //Removing @"id" since it's unique
            int size = [keys_of_dict count];//Incase all values we don't recieve for updation, we update what we get
            for (int i=0; i<size; i++){
                if([_zone_info containsObject:[keys_of_dict objectAtIndex:i]]) {//If matching Values are there
                    sql_query = [sql_query stringByAppendingString:[NSString stringWithFormat:@"%@=?",[keys_of_dict objectAtIndex:i]]];//
                    [values addObject:[new_array objectForKey:[keys_of_dict objectAtIndex:i]]];// ? mark values go here
                    if(i<size-1) sql_query = [sql_query stringByAppendingString:@","];
                }//@end if
            }//@end for values insertion            
            sql_query = [sql_query stringByAppendingString:[NSString stringWithFormat:@" WHERE %@=?",[_zone_info objectAtIndex:1]]];//
            [values addObject:[new_array objectForKey:[_zone_info objectAtIndex:1]]];
            success = [database executeUpdate:sql_query withArgumentsInArray:values];
        }
    }
    else if( [tableName isEqual: [_store_data objectAtIndex:0]] ){
        for(NSDictionary *new_array in jsondata) {
            sql_query =[NSString stringWithFormat:@"UPDATE %@ SET ",tableName];
            NSMutableArray *values = [[NSMutableArray alloc]init];
            NSMutableArray *keys_of_dict = [NSMutableArray arrayWithArray:[new_array allKeys]];
            if([keys_of_dict containsObject:[_store_data objectAtIndex:2]])
                [keys_of_dict removeObject:[_store_data objectAtIndex:2]]; //Removing @"store_id" since it's unique
            int size = [keys_of_dict count];//Incase all values we don't recieve for updation, we update what we get
            for (int i=0; i<size; i++){
                if([_store_data containsObject:[keys_of_dict objectAtIndex:i]]) {//If matching Values are there
                    sql_query = [sql_query stringByAppendingString:[NSString stringWithFormat:@"%@=?",[keys_of_dict objectAtIndex:i]]];//
                    [values addObject:[new_array objectForKey:[keys_of_dict objectAtIndex:i]]];// ? mark values go here
                    if(i<size-1) sql_query = [sql_query stringByAppendingString:@","];
                }//@end if
            }//@end for values insertion
            sql_query = [sql_query stringByAppendingString:[NSString stringWithFormat:@" WHERE %@=?",[_store_data objectAtIndex:2]]];//store_id is at index2
            [values addObject:[new_array objectForKey:[_store_data objectAtIndex:2]]];
            success = [database executeUpdate:sql_query withArgumentsInArray:values];
        }
    }
    else if( [tableName isEqual: [_zone_map objectAtIndex:0]] ){
        for(NSDictionary *new_array in jsondata) {
            sql_query =[NSString stringWithFormat:@"UPDATE %@ SET ",tableName];
            NSMutableArray *values = [[NSMutableArray alloc]init];
            NSMutableArray *keys_of_dict = [NSMutableArray arrayWithArray:[new_array allKeys]];
            if([keys_of_dict containsObject:[_zone_map objectAtIndex:1]])
                [keys_of_dict removeObject:[_zone_map objectAtIndex:1]]; //Removing @"store_id" since it's unique
            int size = [keys_of_dict count];//Incase all values we don't recieve for updation, we update what we get
            for (int i=0; i<size; i++){
                if([_zone_map containsObject:[keys_of_dict objectAtIndex:i]]) {//If matching Values are there
                    sql_query = [sql_query stringByAppendingString:[NSString stringWithFormat:@"%@=?",[keys_of_dict objectAtIndex:i]]];//
                    [values addObject:[new_array objectForKey:[keys_of_dict objectAtIndex:i]]];// ? mark values go here
                    if(i<size-1) sql_query = [sql_query stringByAppendingString:@","];
                }//@end if
            }//@end for values insertion
            sql_query = [sql_query stringByAppendingString:[NSString stringWithFormat:@" WHERE %@=?",[_zone_map objectAtIndex:1]]];//zone_id is at index1
            [values addObject:[new_array objectForKey:[_zone_map objectAtIndex:1]]];
            success = [database executeUpdate:sql_query withArgumentsInArray:values];
        }
    }
    [database close];
    return success;
}


-(void)updateColumnofTable:(NSString*)tableName withColumnName:(NSString*)columnName unique_id:(NSString *)unique_id andValue:(NSString *)value isInteger:(BOOL)isInteger
{   Database *globals = [Database sharedInstance];
    FMDatabase *database = [FMDatabase databaseWithPath:globals.databasePath];
    if(![database open])
        return;
    [database open];
    
    id value_new;
//    NSString *question_mark; //Whether simply ? or '?' while being inserted
    if(isInteger){
        int value_int = [value intValue];
        value_new = [NSNumber numberWithInteger:value_int];
    }
    else value_new = value;
    
    NSString *sql_query;
    if( [tableName isEqual: [_coupon_info objectAtIndex:0]]){
        if([_coupon_info containsObject:columnName])
            sql_query =[NSString stringWithFormat:@"UPDATE %@ SET %@=? WHERE %@=?",tableName, columnName, [_coupon_info objectAtIndex:1]];
        else return;
    }
    else if( [tableName isEqual: [_notification_info objectAtIndex:0]]){
        if([_notification_info containsObject:columnName])
            sql_query =[NSString stringWithFormat:@"UPDATE %@ SET %@=? WHERE %@=?",tableName, columnName, [_notification_info objectAtIndex:1]];
        else return;
    }
    else if( [tableName isEqual: [_sync_data objectAtIndex:0]]){
        if([_sync_data containsObject:columnName])
            sql_query =[NSString stringWithFormat:@"UPDATE %@ SET %@=? WHERE %@=?",tableName, columnName, [_sync_data objectAtIndex:1]];
        else return;
    }
    else if( [tableName isEqual: [_trilytics objectAtIndex:0]]){
        if([_trilytics containsObject:columnName])
            sql_query =[NSString stringWithFormat:@"UPDATE %@ SET %@=? WHERE %@=?",tableName, columnName, [_trilytics objectAtIndex:1]];
        else return;
    }
    
    [database executeUpdate:sql_query withArgumentsInArray:@[value_new, unique_id]];
    [database close];
}

-(void)updateTableResetIS_SEEN{
    Database *globals = [Database sharedInstance];
    FMDatabase *database = [FMDatabase databaseWithPath:globals.databasePath];
    if(![database open])
        return;
    [database open];
    
    NSString *tableName = [_coupon_info objectAtIndex:0];
    NSString *columnName = @"is_seen";
    NSString *sql_query;

        if([_coupon_info containsObject:columnName])
            sql_query =[NSString stringWithFormat:@"UPDATE %@ SET %@=0 WHERE %@=1",tableName, columnName, columnName];
        else return;

    
    [database executeUpdate:sql_query ];
    [database close];
}

//------------------------
#pragma mark - Check Expiry
//------------------
-(BOOL)couponExpiry: (NSString *)expiry_date {
    
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormat setLocale:[NSLocale currentLocale]];
    NSDate *date_of_expiry = [dateFormat dateFromString:expiry_date];
    //..
    NSDate *date_today = [NSDate date];
    if ([date_today compare:date_of_expiry] == NSOrderedDescending )
        return YES; //means expired
    
    return NO; //means valid_coupon
}


//-------------------------------------
#pragma mark - fetching Offers
//-------------------------------------
- (void)jsonFetch: (NSString *)URL_string withCompletionHandler:( void (^)(void) )blockCompletion {
    Database *globals = [Database sharedInstance];
    NSString *API_DATABASE_INFO = URL_string;
    NSURL *icyURL = [NSURL URLWithString:API_DATABASE_INFO];
    
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    NSString *key = [preferences objectForKey:@"sdk_token"] ;
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:icyURL];
    [request setHTTPMethod:@"GET"];
    [request setValue:key forHTTPHeaderField:@"usertoken"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
   
    NSLog(@"\n\ndatbase/jsonFetch-RequestKey : %@\n.", key);
    //    NSLog(@"\nNSDATA : %@", [[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding]);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig delegate: (id)self delegateQueue:nil];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *_Nullable data, NSURLResponse *_Nullable response, NSError *_Nullable error) {
                                      if (error) {
                                          // do something with the erroE
                                           NSLog(@"-- database/jsonFetch ERROR:\n");
                                          blockCompletion();
                                          return;
                                      }
                                      NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                      if (httpResponse.statusCode == 201) {
                                          NSDictionary* arrTokenData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                          NSLog(@"-- database/jsonFetch : %@", arrTokenData);
                                          //Taking payload
                                          NSDictionary *payload = [arrTokenData objectForKey:@"payload"];
                                          [self saveApiResults:payload];
                                          blockCompletion();
                                      } else {
                                          // failure: do something else on failure
                                          NSLog(@"-- database/jsonFetch httpError:\n");
                                          NSLog(@"httpResponse code: %@", [NSString stringWithFormat:@"%ld", (unsigned long)httpResponse.statusCode]);
                                          NSLog(@"httpResponse head: %@", httpResponse.allHeaderFields);
                                          return;
                                      }
                                  }];
    [task resume];
}

//--------------
#pragma mark - TableName constants
//--------------
static NSArray *_coupon_info;
static NSArray *_zone_info;
static NSArray *_notification_info;
static NSArray *_trilytics;
static NSArray *_category;
static NSArray *_coupon_category;
static NSArray *_sync_data;
static NSArray *_store_data;
static NSArray *_zone_map;


//---NOTE : Changes or Extra columns to be made:
//1.here, and in the function
//2.createAndCheckDatabase

+(NSArray*) get_couponinfo {
    _coupon_info = @[@"coupon_info", //<--Table name1
                     @"id",//1
                     @"name",//2
                     @"tone",//Integer
                     @"category",//4
                     @"type",//5
                     @"zone_id",//6
                     @"brand_name",//7
                     @"descp",//8
                     @"image",//9
                     @"expiry", //10
                     @"is_sharable",//Integer
                     @"is_timed",//Integer
                     @"is_used",//Integer
                     @"is_fav",//Integer
                     @"is_seen",//Integer
                     @"code",
                     @"store_id"
                     //Latitude and Longitude have to be removed
//                     @"latitude",
//                     @"longitude"
                     ];//0-17
    return _coupon_info;
}
+(NSArray*) get_zoneinfo {
    _zone_info = @[@"zone_info", //<--Table name2
                     @"zone_id",
                     @"zone_name",
                     @"zone_img",
                     @"actions",
                     @"latitude", //Integer type
                     @"longitude", //Integer type
                     @"descp",
                     @"num_coupon",
                     @"zone_code"
                     ];//0-9
    return _zone_info;
}
+(NSArray*) get_notificationinfo {
    _notification_info = @[@"notification_info", //<--Table name3
                   @"id",
                   @"title",
                   @"message",
                   @"type",
                   @"image",
                   @"zone_id",
                   @"category",
                   @"expiry"
                   ];//0-8
    return _notification_info;
}
+(NSArray*) get_trilytics {
    _trilytics = @[@"trilytics", //<--Table name4
                   @"data_object", //JSON_object
                   ];//0-1
    return _trilytics;
}
+(NSArray*) get_categoryinfo {
    _category = @[@"category_info", //<--Table name5
                   @"category_id",
                   @"category_name",
                  @"category_image"
                  ];//0-3
    return _category;
}
+(NSArray*) get_couponcategory {
    _coupon_category = @[@"coupon_category", //<--Table name6
                  @"coupon_id",
                  @"category_id",
                  @"zone_id"
                  ];//0-3
    return _coupon_category;
}

+(NSArray*) get_syncdata {
    _sync_data = @[@"sync_data", //<--Table name7
                  @"coupon_id",
                  @"action_type",
                  @"action_value",
                  @"timestamp"
                  ];//0-4
    return _sync_data;
}

+(NSArray*) get_storedata {
    _store_data = @[@"store_data", //<--Table name8
                   @"zone_id",
                   @"store_id",
                   @"store_name",
                   @"store_location",
                   @"store_image",
                   @"F_Level"
                   ];//0-6
    return _store_data;
}

+(NSArray*) get_zonemap {
    _zone_map = @[@"zone_map", //<--Table name9
                    @"zone_id",
                    @"floor_name",
                    @"floor_image",
                    @"F_Level",
                    ];//0-4
    return _zone_map;
}


@end
