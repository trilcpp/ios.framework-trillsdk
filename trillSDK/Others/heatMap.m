//
//  heatMap.m
//  trillSDK
//
//  Created by Mac Mini on 12/4/17.
//  Copyright © 2017 TrillBit. All rights reserved.
//

#import "heatMap.h"
#define kDestinationURL @"http://54.169.234.17:8099/user/locate/"

@implementation heatMap

@synthesize deviceID = _deviceID;
@synthesize deviceModel = _deviceModel;
@synthesize zoneID = _zoneID;
@synthesize trigger_chunks = _trigger_chunks;

-(id)init:(NSString *)devID deviceModel:(NSString *)devModel
{
    self = [super init];
    heatMap *globals = [heatMap sharedInstance];
    globals.deviceID = devID;
    globals.deviceModel = devModel;
    globals.zoneID = [[NSMutableArray alloc] init];
    globals.trigger_chunks = [[NSMutableArray alloc] init];
    return self;
}

+ (heatMap *) sharedInstance{
    static dispatch_once_t onceToken;
    static heatMap *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[heatMap alloc] init];
    });
    return instance;
}

//----Called everytime something is decoded
-(void )saveUserData:(int)code : (NSMutableArray *) power_spectrum2D {
    heatMap *globals = [heatMap sharedInstance];
    [globals.zoneID addObject:[NSNumber numberWithInteger:(code)]];
    [globals.trigger_chunks addObjectsFromArray:power_spectrum2D];
    
    int count = [globals.zoneID count];
    if(count >1 && [[globals.zoneID objectAtIndex:(count-2)] integerValue] != code)
        [self uploadUserData:^{
            [globals.zoneID removeAllObjects];
            [globals.trigger_chunks removeAllObjects];
        }];
}


//----Called every minute
-(void)checkCounter{
    heatMap *globals = [heatMap sharedInstance];
    [self uploadUserData:^{
        [globals.zoneID removeAllObjects];
        [globals.trigger_chunks removeAllObjects];
    }];
}


-(void)uploadUserData: (void (^)(void) )completionHandler {
    heatMap *globals = [heatMap sharedInstance];
    
    NSURL *icyURL = [NSURL URLWithString:kDestinationURL];
    NSString *devID = [NSString stringWithFormat: @"%@",globals.deviceID];
    NSString *devMod = [NSString stringWithFormat: @"%@",globals.deviceModel];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:devID forKey:@"device_id"];
    [dict setValue:devMod forKey:@"device_model"];
    [dict setObject:globals.zoneID forKey:@"zone_id" ];
    [dict setObject:globals.trigger_chunks forKey:@"trigger_chunks" ];
    NSLog(@"\n\n+++++ %@", dict);
    NSData *requestData =   [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:icyURL];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: requestData];
    NSLog(@"\nRequest : %@", request);
    NSLog(@"\nNSDATA : %@", [[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding]);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig delegate: (id)self delegateQueue:nil];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *_Nullable data, NSURLResponse *_Nullable response, NSError *_Nullable error) {
                                      if (error) {
                                          // do something with the erroR
                                          NSLog(@"\n\nheatMap ERROR\n");
                                          return;
                                      }
                                      NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                      if (httpResponse.statusCode == 201) {
                                          NSArray* arrTokenData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                          NSLog(@"%@", arrTokenData);
                                          completionHandler();
                                      } else {
                                          // failure: do something else on failure
                                          NSLog(@"\n\nheatMap httpError\n");
                                          NSLog(@"httpResponse code: %@", [NSString stringWithFormat:@"%ld", (unsigned long)httpResponse.statusCode]);
                                          NSLog(@"httpResponse head: %@", httpResponse.allHeaderFields);
                                          completionHandler();
                                          
                                          return;
                                      }
                                  }];
    [task resume];
    
}
@end
