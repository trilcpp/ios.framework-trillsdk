
//
//  trillTech.m
//  trillSDK
//
//  Created by Shivansh on 11/14/17.
//  Copyright © 2017 TrillBit. All rights reserved.
//


#import "trillTech.h"
#import "ShortBuffer.hpp"
#import "heatMap.h"
#import "Database.h"
#import <trillBPP.h>

#import <sys/utsname.h> //For fetching device details
#import <AdSupport/ASIdentifierManager.h>

#define kDestinationURL @"https://api.trillbit.com/v1/authorize/"


//................Audio data format
#define NUM_CHANNELS 1
#define SAMPLE_TYPE SInt16
#define SAMPLE_RATE 44100
#define NUM_BUFFERS 3
#define BUFFER_SIZE 4096    //8192 makes recording of 4096 size

#define TRIGGER_VALUE 85


// Struct defining recording state
typedef struct
{
    AudioStreamBasicDescription  dataFormat;
    AudioQueueRef                queue;
    AudioQueueBufferRef          buffers[NUM_BUFFERS];
    AudioFileID                  audioFile;
    UInt32                        mNumPacketsToRead;
    SInt64                       currentPacket;
    bool                         recording;
} RecordState;

// Struct defining playback state
typedef struct
{
    AudioStreamBasicDescription  dataFormat;
    AudioQueueRef                queue; //The playback audio queue created by your application.
    AudioQueueBufferRef          buffers[NUM_BUFFERS];  //An array holding pointers to the audio queue buffers managed by the audio queue.
    AudioFileID                  audioFile;     //An audio file object that represents the audio file your program plays.
    UInt32                       mNumPacketsToRead; //STOLEN :The number of packets to read on each invocation of the audio queue’s playback callback
    SInt64                       currentPacket; //The packet index for the next packet to play from the audio file.
    bool                         playing;
} PlayState;

//--------------//--------------//--------------//--------------//--------------//
#pragma mark - CLASS SDK
//--------------//--------------//--------------//--------------//--------------//

@interface trillSDK ()
{
    NSMutableArray *trigger_buffer;
    NSMutableArray *power_spectrum2D;
    bool IS_TRIGGER_FOUND; //fuck global
    int empty_sum;
    int chunkSize;
    int windowSize;
    int ratio;
    Database *DB;
    
    int lastTriggerValue; //fuck global..For function TriggerCodeLogic (for non repetive numbers)
    
    RecordState recordState;
    PlayState playState;
    CFURLRef fileURL;
}
@end


// Declare C callback functions
void AudioInputCallback(void * inUserData,  // Custom audio metadata
                        AudioQueueRef inAQ,
                        AudioQueueBufferRef inBuffer,
                        const AudioTimeStamp * inStartTime,
                        UInt32 inNumberPacketDescriptions,
                        const AudioStreamPacketDescription * inPacketDescs);

void AudioOutputCallback(void * inUserData,
                         AudioQueueRef outAQ,
                         AudioQueueBufferRef outBuffer);


#pragma mark

dispatch_queue_t concurrentQueue; //My concurrent thread, for asynchronous task, declared outside so that can be accessed inside callback functions
@implementation trillSDK

- (void)initializeSDK: (NSString *) SDK_key:(NSString *) name_of_package  completionHandler: ( void (^)(void) )complete {
    //----Global array, to store frequency codes(trigger)
    trigger_buffer = [NSMutableArray arrayWithCapacity:6];
    power_spectrum2D = [NSMutableArray arrayWithCapacity:6];
    IS_TRIGGER_FOUND =false;
    empty_sum = 0;
    chunkSize = 2048;
    windowSize = 1024;
    ratio = chunkSize/windowSize;
    
    //----Analytics, call every 1 minute(60sec)
//    heatMap *HM = [[heatMap alloc] init:[[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString] deviceModel:[self device_name] ];
//    [NSTimer scheduledTimerWithTimeInterval:60.0f target:HM selector:@selector(checkCounter) userInfo:nil repeats:YES];
    
    //----Check Package Validity
    BOOL valid_package = [self check_SharedPreferences];
    if (valid_package == true)
    { complete();
      return;
    }
    Database *DB = [[Database alloc] init];
    
    //----Fetch sdk_token from server
    [self sendServer:SDK_key :name_of_package completionHandler:^{
        //----Database Operation, check if database exists
        NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
        NSString *sdk_token = [preferences objectForKey:@"sdk_token"] ;
        [DB loadDatabase: sdk_token];
        complete();
    }];
    
}

//@synthesize triggerResponseBlock;
- (void) infoCallBack
{
    //-----Check SharedPreferences and package validity
    BOOL valid_package = [self check_SharedPreferences];
    if(valid_package == false) {
        NSLog(@"\n\n--SDK_message: (-) App.Package Invalid\n\n");
        return;
    }
    NSLog(@"\n\n--SDKmessage: App.Package Valid\n\n");
    
    //-----ViewDidLoad of AudioServerQueue PROJECT
    char path[256];
    [self getFilename:path maxLenth:sizeof path];
    fileURL = CFURLCreateFromFileSystemRepresentation(NULL, (UInt8*)path, strlen(path), false);

    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error:nil];
    //Init state variables
    playState.playing = false;
    recordState.recording = false;
    //BOOL didFinishLaunchingWithOptions
    concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    trillSDK *t = self; //For reftoSelf
    refToSelf = t;
    //---Start Recording
    check_result_flag = 100;
    [self startRecording];
}

- (void)SDKdestructor{
    check_result_flag = 0;
    [self stopRecording];
}

-(void) TriggerCodeLogic: (int) trigger_code {
    if( trigger_code > 0 && lastTriggerValue!=trigger_code){
        [trigger_buffer addObject:[NSNumber numberWithInteger:trigger_code]];
        lastTriggerValue = trigger_code;
    }
    
    int buffer_ct = [trigger_buffer count];
    
    
    if( buffer_ct == 5){
        bool isLocation = ( [[trigger_buffer objectAtIndex:0]integerValue]==1 ? true : false );
        int code_array[4];
        
        //Check whether ZoneTrigger or not, AND mark interaction_code starter
        int ct=0;
        for(int i =[trigger_buffer count]-1; i>=1;i--)
            code_array[ct++] = [[trigger_buffer objectAtIndex:i]integerValue];
        
        int mark_index_interaction_code=0; //ASSUME, 5 occurs first. no matter what, hence ct=0
        int mul = 1;
        for(int i=0; i< 4 ; i++, mul*=10){
            mark_index_interaction_code += mul*code_array[i]   ;
        }
        
        if([self.delegate respondsToSelector:@selector(onToneDecrypted::)]){
            [self.delegate onToneDecrypted:mark_index_interaction_code:isLocation];
        }
        
        [trigger_buffer removeAllObjects];
        lastTriggerValue = -1;
        
    }//@ENDof if(count==6)
    
}

//- (void) TriggerCodeLogic{
//    NSMutableArray *finalNumberArray = [NSMutableArray arrayWithCapacity:6];
//    heatMap *HM = [[heatMap alloc] init];
//    bool isLocation = false ;
//
//    for(int i = 1; i<12; i+=2)
//    {   NSArray *small_chunk_array=[trigger_buffer objectAtIndex:i];
//        NSArray *small_power_array=[power_spectrum2D objectAtIndex:i];
////        NSLog(@"array small : %@ \n Power: %@", small_chunk_array, small_power_array);
//        // intit counter for each freq, right now we have 5 frequencies.
//        int counter[5] ={0,0,0,0,0};
//        // increase the counter for each freq occurrence.
//        // Counting the occurrence of each freqency in bin.
//        for (int j = 0 ; j<ratio; j++){
//            int freq = [[small_chunk_array objectAtIndex:j] integerValue];
//            if( freq >0) counter[freq-1]+=1;
//        }
//        // checking for maximum counter freq.
//        int decoded = 0;
//        for (int j=0; j< 5; j++) {
//            if (counter[j] > decoded) {
//                decoded = j+1;
//            } else if (counter[j] == decoded && decoded != 0) {
//                int localmax = [[small_power_array objectAtIndex:0] integerValue];
//                for (int k= 1; k< ratio; k++) {
//                    if ([[small_power_array objectAtIndex:k] integerValue] > localmax) {
//                        localmax = [[small_power_array objectAtIndex:k] integerValue];
//                        decoded = [[small_chunk_array objectAtIndex:k] integerValue];
//                    }
//                }
//            }//--else if
//        }//--for
//        [finalNumberArray addObject:[NSNumber numberWithInteger:decoded] ];
//        if([finalNumberArray count]==6)
//            break;
//    }//FOR i statement
//    //----If code starts with 55 or 11, only then parse
//    int firstVal = (int)[[finalNumberArray objectAtIndex:0]integerValue];
//    int secondVal = (int)[[finalNumberArray objectAtIndex:1]integerValue];
//    if( (firstVal==5 and secondVal==5) || (firstVal==1 and secondVal==1)) {
//        int interaction_code=0;
//        int mul=1;
//        for(int i=5; i>=2 ; mul*=10, i--){
//            interaction_code = interaction_code + mul * [[finalNumberArray objectAtIndex:i] integerValue];
//        }
//        //-- If 11
//        if([[finalNumberArray objectAtIndex:0]integerValue]==1){
//            [HM saveUserData:interaction_code : power_spectrum2D];
//            isLocation = true;
//        }
//        //-- Check database for code
//
////        [DB loadDatabase:@"4d8c10f9-3b80-48a9-902e-a78dace8963e"];
//        NSDictionary *dataDictionary = [[NSDictionary alloc]init];
//        dataDictionary = [DB fetchCodeDictionary:interaction_code];
//        //-- delegate
//        if(  [self.delegate respondsToSelector:@selector(onToneDecrypted::)] ){
//            [self.delegate onToneDecrypted:interaction_code :isLocation];
//            [self.delegate onDataRecieved:interaction_code :dataDictionary];
//        }
//    }
//    [trigger_buffer removeAllObjects];
//    [power_spectrum2D removeAllObjects];
//
//}

//------------------------------------------------------------------------------
#pragma mark - AUDIO FUNCTIONS
//---------------------------------
#pragma mark - feedSamples
//------------------------------------------------------------------------------
id refToSelf;
int ct=0;
int check_result_flag; //initial in infoCallBack()
ShortBuffer strbuffer;
//@ct -> Our own count for circular buffer for storing "shorts" in string, value goes upto @STORAGE_VECTOR
//@check_result_flag -> Gives back OTP result as well as other flags
//                   0 -> Continue to check trigger values for every @BUFFER_SIZE "shorts"
//                  -3 -> When trigger value is detected
//                  -4 -> When the next @TOTAL_CHUNKS_HAVING_DATA are being stored after detecting -3
//                  -5 -> When the final buffer is stored, so that ASYNCHRONOUS thread can be run when CORRELATION calculations will happen
//                  -2 -> When calculation for OTP is going on
//                  >0 -> OTP detected ; -1 -> Wrong OTP

- (void)feedSamplesToEngine:(UInt32)audioDataBytesCapacity audioData:(void *)audioData
{
    if(audioDataBytesCapacity > BUFFER_SIZE)
        return; //#Handles error where overflow is there.
    
    int sampleCount = audioDataBytesCapacity / sizeof(SAMPLE_TYPE);
    SAMPLE_TYPE *samples = (SAMPLE_TYPE*)audioData;
    std::string shorts;
    double power = pow(2,10);
    for(int i = 0; i < sampleCount; i++) {
        //#EXTREME IMPORTANCE: the data recieved is in a format where the endianess,i.e, format of binary is in a different way
        //This is done to correct that error for "16BIT" binary number
        SAMPLE_TYPE sample_swap =  (0xff00 & (samples[i] << 8)) | (0x00ff & (samples[i] >> 8)) ; //Endianess issue
        char dataInterim[30];
        sprintf(dataInterim,"%f ", sample_swap/power); // normalize it.
        shorts.append(dataInterim);
    }
    //------Checking trigger
    if(check_result_flag == 100) {
        Trigger *tg = (Trigger *)malloc(sizeof(class Trigger)); //initiated for later use
        int decoded_frequency[5];
        int power_percentage[5];
        int decoded_trigger = -1;

        tg->windowCall(shorts, chunkSize, windowSize, decoded_frequency, power_percentage);
        
        for (int i =0; i<ratio; i++){
            decoded_trigger = (decoded_frequency[i]>0) ? decoded_frequency[i]: decoded_trigger;
//------Enable for latest logic, which has been commented out in its function as well(TriggerCodeLogic)
//            IS_TRIGGER_FOUND = (decoded_trigger==5 || decoded_trigger==1 )? (empty_sum=0 or true ):IS_TRIGGER_FOUND;
//------Disable if enabling above
            IS_TRIGGER_FOUND = (decoded_trigger>0)? (empty_sum=0 or true ):IS_TRIGGER_FOUND;
        }
        //---Conditions
        if(IS_TRIGGER_FOUND){
            NSMutableArray *frequency= [NSMutableArray arrayWithCapacity:5];
            NSMutableArray *power = [NSMutableArray arrayWithCapacity:5];
            for (int i =0; i<ratio; i++) [frequency addObject:[NSNumber numberWithInteger:decoded_frequency[i]]];
            for (int i =0; i<ratio; i++) [power addObject:[NSNumber numberWithInteger:power_percentage[i]]];
            
//----Enable for latest logic, which has been commented out in its function as well(TriggerCodeLogic)
//            [trigger_buffer addObject:frequency];
//            [power_spectrum2D addObject:power];
//            if([trigger_buffer count]==12){
////                NSLog(@"array full : %@ \n", trigger_buffer);
//                [self TriggerCodeLogic];
//                IS_TRIGGER_FOUND = false;
//            }
            
            //---Disable if enabling above
            [self TriggerCodeLogic: decoded_trigger];
            
        }
        
        
        if (decoded_trigger == -1) empty_sum+=1;
        
        if(empty_sum==5)  //--Clear Buffer
        {   [trigger_buffer removeAllObjects];
            [power_spectrum2D removeAllObjects];
            IS_TRIGGER_FOUND = false;
            lastTriggerValue = -1;
        }
    
        free(tg);
    } //end if
    //---------Send for decryption
    else {
        int send_to_corr;
        send_to_corr = strbuffer.putshortsinarray(shorts, ct, 90);
        check_result_flag = send_to_corr;
        dispatch_async(dispatch_get_main_queue(), ^{
            if(check_result_flag == -2 ){
                [self stopRecording];
                NSLog(@"Stopping recording, entry in -2");
            }
            if(check_result_flag>0){
                if(  [self.delegate respondsToSelector:@selector(onToneDecrypted: )] )
                    [self.delegate onToneDecrypted:check_result_flag: false];
                
                [self startRecording];
                
                check_result_flag = 100; //Enable checking triggers again
                }
            else if (check_result_flag==-1){
                check_result_flag = 100;
            }
        });
    }//Else statement
}

//------------------------------------------------------------------------------
#pragma mark - CallBack
//------------------------------------------------------------------------------
//.........................................................................................................
//##########################################################################################

// Takes a filled buffer and writes it to disk, "emptying" the buffer
void AudioInputCallback(void * inUserData,
                        AudioQueueRef inAQ,
                        AudioQueueBufferRef inBuffer,
                        const AudioTimeStamp * inStartTime,
                        UInt32 inNumberPacketDescriptions,
                        const AudioStreamPacketDescription * inPacketDescs)
{
    RecordState * recordState = (RecordState*)inUserData;
    if (!recordState->recording)
    {
        NSLog(@"%@","--SDK_message : Audio InputCallBack Not recording, returning\n");
        return; //important otherwise takes global value of check in its other buffers.
    }
    //     if (inNumberPacketDescriptions == 0 && recordState->dataFormat.mBytesPerPacket != 0)
    //     {
    //         inNumberPacketDescriptions = inBuffer->mAudioDataByteSize / recordState->dataFormat.mBytesPerPacket;
    //     }
//    printf("Writing buffer %lld\n", recordState->currentPacket);
//    OSStatus status = AudioFileWritePackets(recordState->audioFile,
//                                            false,
//                                            inBuffer->mAudioDataByteSize,
//                                            inPacketDescs,
//                                            recordState->currentPacket,
//                                            &inNumberPacketDescriptions,
//                                            inBuffer->mAudioData);
//    if (status == 0) {
//        recordState->currentPacket += inNumberPacketDescriptions;
//    }
//    else NSLog(@"\nStatus AudioInputCallBack: %d",(int)status);
    
    AudioQueueEnqueueBuffer(recordState->queue, inBuffer, 0, NULL);
    //Do stuff with samples recorded
    if(check_result_flag == -4 ){
        [refToSelf feedSamplesToEngine:inBuffer->mAudioDataBytesCapacity audioData:inBuffer->mAudioData];
            }
    else {
//        dispatch_async(concurrentQueue, ^{
            [refToSelf feedSamplesToEngine:inBuffer->mAudioDataBytesCapacity audioData:inBuffer->mAudioData];
//        });
    }
}


//.##########################################################################################..

//------------------------------------------------------------------------------
#pragma mark - AudioFormat
//------------------------------------------------------------------------------

- (void)setupAudioFormat:(AudioStreamBasicDescription*)format
{
    //SAMPLE_TYPE is short datatype
    format->mSampleRate = SAMPLE_RATE;
    format->mFormatID = kAudioFormatLinearPCM;
    format->mFramesPerPacket = 1;
    format->mChannelsPerFrame = 1;
    format->mBytesPerFrame = sizeof(SAMPLE_TYPE) * NUM_CHANNELS; //2;
    format->mBytesPerPacket = sizeof(SAMPLE_TYPE) * NUM_CHANNELS;    //2;
    format->mBitsPerChannel = 8 * sizeof(SAMPLE_TYPE);          //16;
    format->mReserved = 0;
    format->mFormatFlags = kLinearPCMFormatFlagIsSignedInteger | kLinearPCMFormatFlagIsPacked | kLinearPCMFormatFlagIsBigEndian ;
}


//#############################################
#pragma mark - Start and stop
//#############################

- (void)startRecording
{
    [self setupAudioFormat:&recordState.dataFormat];
    NSLog(@"\n--SDK_message : Sample : %f",recordState.dataFormat.mSampleRate);
    
    recordState.currentPacket = 0;
    
    
    OSStatus status;
    status = AudioQueueNewInput(&recordState.dataFormat,
                                AudioInputCallback,
                                &recordState,
                                CFRunLoopGetCurrent(),
                                kCFRunLoopCommonModes,
                                0,
                                &recordState.queue);
    if (status == 0)
    {
        // Prime recording buffers with empty data
        for (int i = 0; i < NUM_BUFFERS; i++)
        {
            AudioQueueAllocateBuffer(recordState.queue, BUFFER_SIZE , &recordState.buffers[i]);
            AudioQueueEnqueueBuffer (recordState.queue, recordState.buffers[i], 0, NULL);
        }
        
        status = AudioFileCreateWithURL(fileURL, //if returns -50, then fileURL= " "should have path
                                        kAudioFileAIFFType,
                                        &recordState.dataFormat,
                                        kAudioFileFlags_EraseFile,
                                        &recordState.audioFile);
        
        if (status == 0)
        {
            recordState.recording = true;
            status = AudioQueueStart(recordState.queue, NULL);
            if (status == 0)
            {
                NSLog(@"\n--SDK_message : Recording STARTED");
            }
        }
    }
    
    if (status != 0)
    {
        NSLog(@"\n--SDK_message :Status startRecording: %d \n",(int)status);
        [self stopRecording];
        NSLog(@"--SDK_message : Failed Recording");
    }
    
    
}

- (void)stopRecording
{
    recordState.recording = false;
    AudioQueueStop(recordState.queue, true);
    for(int i = 0; i < NUM_BUFFERS; i++)
    {
        AudioQueueFreeBuffer(recordState.queue, recordState.buffers[i]);
    }
    
    AudioQueueDispose(recordState.queue, true);
    AudioFileClose(recordState.audioFile);
    NSLog(@"\n--SDK_message : StopRecording - idle State");
}


//------------------------------------------------------------------------------
#pragma mark - END
//------------------------------------------------------------------------------

- (BOOL)getFilename:(char*)buffer maxLenth:(int)maxBufferLength
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString* docDir = [paths objectAtIndex:0];
    NSString* file = [docDir stringByAppendingString:@"/recording.aif]"];
    return [file getCString:buffer maxLength:maxBufferLength encoding:NSUTF8StringEncoding];
}


//------------------------------------------------------------------------------
#pragma mark - JSON callmethod body
//------------------------------------------------------------------------------
- (void) sendServer: (NSString *)API_KEY : (NSString *)name_package completionHandler:( void (^)(void) )blockCompletion {
NSURL *icyURL = [NSURL URLWithString:kDestinationURL];
NSString *key = [NSString stringWithFormat: @"%@", API_KEY];
NSString *pack_name = [NSString stringWithFormat: @"%@",name_package];
NSString *adID = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];

NSDictionary *dict = [[NSMutableDictionary alloc]init];
[dict setValue:key forKey:@"sdk_key"];
[dict setValue:pack_name forKey:@"package_name"];
[dict setValue: [self device_name] forKey:@"device_modal"];
[dict setValue: adID forKey:@"device_id"];
//NSData *requestData = [NSData dataWithBytes:[vecArray UTF8String] length:[vecArray lengthOfBytesUsingEncoding:NSUTF8StringEncoding]];
NSData *requestData =   [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];

NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:icyURL];
[request setHTTPMethod:@"POST"];
[request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
[request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
[request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
[request setHTTPBody: requestData];
NSLog(@"\n\n--SDK_message - Request Data being sentToServer");
NSLog(@"\nData being sent NSDATA : %@", [[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding]);

NSURLSessionConfiguration *sessionConfig =
[NSURLSessionConfiguration defaultSessionConfiguration];
NSURLSession *session =
[NSURLSession sessionWithConfiguration:sessionConfig delegate: (id)self delegateQueue:nil];
NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                        completionHandler:
                              ^(NSData *_Nullable data, NSURLResponse *_Nullable response, NSError *_Nullable error) {
                                  if (error) {
                                      // do something with the erroE
                                      return;
                                  }
                                  NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                  if (httpResponse.statusCode == 201) {
                                      NSDictionary* arrTokenData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                      NSLog(@"%@", arrTokenData);
                                      [self WritingSharedPreferences:arrTokenData];
                                      blockCompletion();
                                  } else {
                                      // failure: do something else on failure
                                      NSLog(@"\n\n--SDK_message : httpResponse code SDK check:\n %@", [NSString stringWithFormat:@"%ld", (unsigned long)httpResponse.statusCode]);
                                      NSLog(@"\nhttpResponse head: %@\n\n", httpResponse.allHeaderFields);
                                      return;
                                  }
                              }];
[task resume];
}

-(void) WritingSharedPreferences : (NSDictionary*) jsondata {
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    
    NSDictionary *payload = [jsondata objectForKey:@"payload"];
    NSString *next = [NSString stringWithFormat:@"%@", [payload objectForKey:@"next"]];
    NSString *sdk_token = [NSString stringWithFormat:@"%@", [payload objectForKey:@"sdk_token"]];
    
    [preferences setObject:next forKey:@"next"];
    [preferences setObject:sdk_token forKey:@"sdk_token"];
    
    //  Save to disk
    const BOOL didSave = [preferences synchronize];
    
    if (!didSave)
    {
        NSLog(@"\n--SDK_message : error : UserDefaults saving token Writing");
    }
    
}

-(BOOL) check_SharedPreferences {
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    
    NSString *next_validity = [preferences objectForKey:@"next"] ;
    NSString *token_validity = [preferences objectForKey:@"sdk_token"] ;
    if (next_validity == nil || token_validity == nil) 
        return false;
    else
    {   NSString *next_date =[preferences objectForKey:@"next"];
        // Convert string to date object
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *expiry_date = [dateFormat dateFromString:next_date];
        //..
        NSDate *date_today = [NSDate date];
        NSLog(@"\n--SDK_message : Token valid till : %@", expiry_date);
        
        if ([date_today compare:expiry_date] == NSOrderedDescending )
        {
            NSLog(@"\n--SDK_message : Unverified : Expired Token");
            return false;
        }
        return true;
    }
}



-(NSString*)device_name {
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString *platform= [NSString stringWithCString:systemInfo.machine
                                           encoding:NSUTF8StringEncoding];
    
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"Verizon iPhone 4";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5 GSM";
    if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone 5c (GSM)";
    if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone 5c (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone 5s GSM";
    if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone 5s (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    if ([platform isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
    if ([platform isEqualToString:@"iPhone8,1"])    return @"iPhone 6s";
    if ([platform isEqualToString:@"iPhone8,2"])    return @"iPhone 6s Plus";
    if ([platform isEqualToString:@"iPhone8,4"])    return @"iPhone SE";
    if ([platform isEqualToString:@"iPhone9,1"])    return @"iPhone 7";
    if ([platform isEqualToString:@"iPhone9,2"])    return @"iPhone 7 Plus";
    if ([platform isEqualToString:@"iPhone9,3"])    return @"iPhone 7";
    if ([platform isEqualToString:@"iPhone9,4"])    return @"iPhone 7 Plus";
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([platform isEqualToString:@"iPod5,1"])      return @"iPod Touch 5G";
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([platform isEqualToString:@"iPad2,4"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,5"])      return @"iPad Mini (WiFi)";
    if ([platform isEqualToString:@"iPad2,6"])      return @"iPad Mini (GSM)";
    if ([platform isEqualToString:@"iPad2,7"])      return @"iPad Mini (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3 (WiFi)";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3 (GSM)";
    if ([platform isEqualToString:@"iPad3,4"])      return @"iPad 4 (WiFi)";
    if ([platform isEqualToString:@"iPad3,5"])      return @"iPad 4 (GSM)";
    if ([platform isEqualToString:@"iPad3,6"])      return @"iPad 4 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad4,1"])      return @"iPad Air (WiFi)";
    if ([platform isEqualToString:@"iPad4,2"])      return @"iPad Air (Cellular)";
    if ([platform isEqualToString:@"iPad4,3"])      return @"iPad Air";
    if ([platform isEqualToString:@"iPad4,4"])      return @"iPad Mini 2G (WiFi)";
    if ([platform isEqualToString:@"iPad4,5"])      return @"iPad Mini 2G (Cellular)";
    if ([platform isEqualToString:@"iPad4,6"])      return @"iPad Mini 2G";
    if ([platform isEqualToString:@"iPad4,7"])      return @"iPad Mini 3 (WiFi)";
    if ([platform isEqualToString:@"iPad4,8"])      return @"iPad Mini 3 (Cellular)";
    if ([platform isEqualToString:@"iPad4,9"])      return @"iPad Mini 3 (China)";
    if ([platform isEqualToString:@"iPad5,3"])      return @"iPad Air 2 (WiFi)";
    if ([platform isEqualToString:@"iPad5,4"])      return @"iPad Air 2 (Cellular)";
    if ([platform isEqualToString:@"AppleTV2,1"])   return @"Apple TV 2G";
    if ([platform isEqualToString:@"AppleTV3,1"])   return @"Apple TV 3";
    if ([platform isEqualToString:@"AppleTV3,2"])   return @"Apple TV 3 (2013)";
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    return platform;
}


//------------------------------------------------------------------------------
#pragma mark - CALLBACK DELEGATE
//---------------------------------
@end

