/*
 * trigger.h
 *
 *  Created on: 27-May-2017
 *      Author: shivansh
 */

#ifndef TRIGGER_H_
#define TRIGGER_H_

#include <trillBPP/vec.h>
#include <trillBPP/transforms.h>
#include <trillBPP/miscfunc.h>
//#include <numeric>

using namespace trill;
using namespace std;


class Trigger {
public:
    void isFound(cvec input, float *result_array);
};

#endif /* TRIGGER_H_ */
