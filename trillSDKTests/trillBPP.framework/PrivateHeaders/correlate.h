/*
 * correlate.h
 *
 *  Created on: Dec 21, 2016
 *      Author: shivansh
 */

#ifndef CORRELATE_H_
#define CORRELATE_H_

#include <trillBPP/vec.h>
#include <trillBPP/correlate.h>

using namespace trill;

class Correlate {
public:
    vec apply(vec data, vec sync);
};

#endif /* CORRELATE_H_ */
